var n = getApp();

Page({
    data: {},
    onLoad: function() {
        var o = this;
        n.util.request({
            url: "manage/order/assign/index",
            data: {},
            success: function(t) {
                var e = t.data.message;
                e.errno ? n.util.toast(e.message) : o.setData(e.message);
            }
        });
    },
    onJsEvent: function(o) {
        n.util.jsEvent(o);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onLoad(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});