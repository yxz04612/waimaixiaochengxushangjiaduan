var e = getApp();

Page(function(e, t, a) {
    return t in e ? Object.defineProperty(e, t, {
        value: a,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : e[t] = a, e;
}({
    data: {
        status: 1,
        refresh: 0,
        showLoading: !1,
        orders: {
            page: 1,
            psize: 10,
            loaded: !1,
            data: []
        },
        showGoods: !1
    },
    onJsEvent: function(t) {
        e.util.jsEvent(t);
    },
    onLoad: function(t) {
        var a = this;
        t && t.sid && e.util.setSid(t.sid), e.util.uploadOpenid(), a.onReachBottom();
    },
    onChangeOrderStatus: function(t) {
        var a = this, s = t.currentTarget.dataset, r = s.type, o = s.id;
        s.status;
        if ("cancel" == r || "direct_deliveryer" == r) return wx.navigateTo({
            url: "./op?type=" + r + "&id=" + o
        }), !1;
        "handle" == r && 1 == s.is_reserve && (s.confirm = s.reserve_confirm), wx.showModal({
            title: "系统提示",
            content: s.confirm,
            success: function(t) {
                t.confirm ? e.util.request({
                    url: "manage/order/takeout/status",
                    data: s,
                    success: function(t) {
                        var s = t.data.message;
                        e.util.toast(s.message, "", 1e3), s.errno || a.onPullDownRefresh();
                    }
                }) : t.cancel;
            }
        });
    },
    onPushUU: function(t) {
        e.util.request({
            url: "manage/order/takeout/push_uupaotui",
            data: {
                id: t.currentTarget.dataset.id,
                push: 0
            },
            method: "POST",
            success: function(t) {
                var a = t.data.message;
                if (a.errno) return e.util.toast(a.message), !1;
                wx.showModal({
                    title: "系统提示",
                    content: a.message.tips,
                    success: function(t) {
                        t.confirm ? e.util.request({
                            url: "manage/order/takeout/push_uupaotui",
                            data: {
                                id: a.message.id,
                                push: 1
                            },
                            method: "POST",
                            success: function(t) {
                                var a = t.data.message;
                                return e.util.toast(a.message, "", 1e3), !1;
                            }
                        }) : t.cancel;
                    }
                });
            }
        });
    },
    onReady: function() {},
    onShow: function() {
        e.util.getStorageSync("order_refresh") && (e.util.removeStorageSync("order_refresh"), 
        this.setData({
            refresh: 1,
            status: 1
        }), this.onReachBottom());
    },
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.data.refresh = 1, this.onReachBottom(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {
        var t = this;
        if (1 == t.data.refresh && (t.data.orders = {
            page: 1,
            psize: 10,
            loaded: !1,
            data: []
        }), t.data.orders.loaded) return !1;
        t.setData({
            showLoading: !0
        }), e.util.request({
            url: "manage/order/takeout/list",
            data: {
                status: t.data.status,
                page: t.data.orders.page,
                psize: t.data.orders.psize
            },
            success: function(a) {
                var s = a.data.message;
                if (s.errno) return e.util.toast(s.message), !1;
                var r = t.data.orders.data.concat(s.message.orders);
                if (t.data.orders.data = r, s.message.orders.length < t.data.orders.psize && (t.data.orders.loaded = !0), 
                t.data.orders.page++, t.data.refresh = 0, t.setData({
                    orders: t.data.orders,
                    showLoading: !1,
                    store: s.message.store
                }), s.message.openid_wxapp_manager) {
                    var o = e.util.getStorageSync("clerkInfo");
                    o.openid_wxapp_manager || (o.openid_wxapp_manager = s.message.openid_wxapp_manager, 
                    e.util.setStorageSync("clerkInfo", o));
                }
            }
        });
    },
    onChangeStatus: function(e) {
        var t = this, a = e.currentTarget.dataset.status;
        t.data.status != a && (t.data.refresh = 1), t.setData({
            status: a
        }), t.onReachBottom();
    },
    onShowGoods: function(e) {
        var t = e.currentTarget.dataset.index;
        this.data.orders.data[t].showGoods = !this.data.orders.data[t].showGoods, this.setData({
            "orders.data": this.data.orders.data
        });
    },
    onShareAppMessage: function() {}
}, "onPullDownRefresh", function() {
    var e = this;
    e.data.refresh = 1, e.onReachBottom(), wx.stopPullDownRefresh();
}));