var a = getApp();

Page({
    data: {
        showLoading: !1,
        records: {
            page: 1,
            psize: 15,
            empty: !1,
            loaded: !1,
            data: []
        }
    },
    onLoad: function(e) {
        var t = this;
        if (!e.id) return a.util.toast("参数错误"), !1;
        t.data.options = e, t.onReachBottom();
    },
    onPullDownRefresh: function() {
        var a = this;
        a.setData({
            records: {
                page: 1,
                psize: 15,
                empty: !1,
                loaded: !1,
                data: []
            }
        }), a.onReachBottom(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {
        var e = this;
        if (e.data.records.loaded) return !1;
        e.setData({
            showLoading: !0
        }), a.util.request({
            url: "manage/order/assign/record",
            data: {
                id: e.data.options.id,
                page: e.data.records.page,
                psize: e.data.records.psize
            },
            success: function(t) {
                var o = t.data.message;
                if (o.errno) a.util.toast(o.message); else {
                    o = o.message;
                    var d = e.data.records.data.concat(o.records);
                    e.data.records.data = d, e.data.records.page++, d.length || (e.data.records.empty = !0), 
                    o.records.length < e.data.records.psize && (e.data.records.loaded = !0), e.setData({
                        records: e.data.records,
                        showLoading: !1,
                        title: o.queue_title,
                        id: e.data.options.id
                    });
                }
            }
        });
    },
    onJsEvent: function(e) {
        a.util.jsEvent(e);
    }
});