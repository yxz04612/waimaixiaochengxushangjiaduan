var t = getApp();

Page({
    data: {
        showStatus: !1
    },
    onLoad: function(e) {
        var a = this;
        t.util.request({
            url: "manage/order/takeout/detail",
            data: {
                id: e.id || 433
            },
            success: function(e) {
                var s = e.data.message;
                s.errno ? t.util.toast(s.message) : a.setData(s.message);
            }
        });
    },
    chooseStatus: function() {
        this.setData({
            showStatus: !this.data.showStatus
        });
    },
    onJsEvent: function(e) {
        t.util.jsEvent(e);
    },
    onChangeOrderStatus: function(e) {
        var a = this, s = e.currentTarget.dataset, u = s.type, o = s.id;
        s.status;
        if ("cancel" == u || "direct_deliveryer" == u || "reply" == u) return wx.navigateTo({
            url: "./op?type=" + u + "&id=" + o
        }), !1;
        "handle" == u && 1 == s.is_reserve && (s.confirm = s.reserve_confirm), wx.showModal({
            title: "系统提示",
            content: s.confirm,
            success: function(e) {
                e.confirm ? t.util.request({
                    url: "manage/order/takeout/status",
                    data: s,
                    success: function(e) {
                        var s = e.data.message;
                        t.util.toast(s.message, "", 1e3), s.errno || a.onLoad({
                            id: o
                        });
                    }
                }) : e.cancel;
            }
        });
    },
    onPushUU: function(e) {
        t.util.request({
            url: "manage/order/takeout/push_uupaotui",
            data: {
                id: e.currentTarget.dataset.id,
                push: 0
            },
            method: "POST",
            success: function(e) {
                var a = e.data.message;
                if (a.errno) return t.util.toast(a.message), !1;
                wx.showModal({
                    title: "系统提示",
                    content: a.message.tips,
                    success: function(e) {
                        e.confirm ? t.util.request({
                            url: "manage/order/takeout/push_uupaotui",
                            data: {
                                id: a.message.id,
                                push: 1
                            },
                            method: "POST",
                            success: function(e) {
                                var a = e.data.message;
                                return t.util.toast(a.message, "", 1e3), !1;
                            }
                        }) : e.cancel;
                    }
                });
            }
        });
    },
    onPullDownRefresh: function() {
        this.onLoad({
            id: this.data.order.id
        }), wx.stopPullDownRefresh();
    }
});