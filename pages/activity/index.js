var t = getApp();

Page({
    data: {},
    onLoad: function() {
        var e = this;
        t.util.request({
            url: "manage/activity/index",
            success: function(s) {
                var n = s.data.message;
                if (n.errno) return t.util.toast(n.message), !1;
                e.setData(n.message);
            }
        });
    },
    onJsEvent: function(e) {
        t.util.jsEvent(e);
    },
    onPullDownRefresh: function() {
        this.onLoad(), wx.stopPullDownRefresh();
    }
});