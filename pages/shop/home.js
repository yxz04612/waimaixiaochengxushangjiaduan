var n = getApp();

Page({
    data: {},
    onLoad: function() {
        var t = this;
        n.util.request({
            url: "manage/shop/index/index",
            success: function(e) {
                var o = e.data.message;
                if (o.errno) return n.util.toast(o.message), !1;
                t.setData(o.message), wx.setNavigationBarTitle({
                    title: o.message.store.title
                });
            }
        });
    },
    onJsEvent: function(t) {
        n.util.jsEvent(t);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onLoad(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});