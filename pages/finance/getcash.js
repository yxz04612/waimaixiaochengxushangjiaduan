var t = getApp();

Page({
    data: {
        submit: !0,
        channel: "weixin"
    },
    onLoad: function(a) {
        var e = this;
        t.util.request({
            url: "manage/finance/getcash",
            success: function(a) {
                var s = a.data.message;
                s.errno ? t.util.toast(s.message) : e.setData({
                    account: s.message.account,
                    config: s.message.config,
                    submit: !1
                });
            }
        });
    },
    onSubmit: function(a) {
        var e = this, s = parseFloat(a.detail.value.fee), n = e.data.account;
        if (e.data.submit) return !1;
        if (isNaN(s)) return t.util.toast("提现金额有误"), !1;
        if (s > n.amount) return t.util.toast("提现金额不能大于账户可用余额"), !1;
        if (s < n.fee_limit) return t.util.toast("提现金额不能小于" + n.fee_limit + "元"), !1;
        var i = (s * n.fee_rate / 100).toFixed(2);
        i = Math.max(i, n.fee_min), n.fee_max > 0 && (i = Math.min(i, n.fee_max)), i = parseFloat(i);
        var u = (s - i).toFixed(2), o = "提现金额" + s + "元, 手续费" + i + "元,实际到账" + u + "元, 确定提现吗";
        wx.showModal({
            content: o,
            success: function(n) {
                n.confirm ? (e.setData({
                    submit: !0
                }), t.util.request({
                    url: "manage/finance/getcash/getcash",
                    methods: "POST",
                    data: {
                        fee: s,
                        formid: a.detail.formId,
                        channel: e.data.channel
                    },
                    success: function(a) {
                        var s = a.data.message;
                        if (s.errno) return t.util.toast(s.message), void e.setData({
                            submit: !1
                        });
                        t.util.toast(s.message, "/pages/shop/setting", 3e3);
                    }
                })) : n.cancel;
            }
        });
    },
    onChangeType: function(t) {
        var a = t.currentTarget.dataset.channel;
        this.setData({
            channel: a
        });
    }
});