var t = getApp();

Page({
    data: {
        delivery: {
            empty: !1,
            list:[
            ],
        }
    },
    onLoad: function() {
        var a = this;
        t.util.request({
            url: "manage/delivery/delivery/list",
            success: function(e) {
                var n = e.data.message;
                if (n.errno) return t.util.toast(n.message), !1;
                n = n.message.data;
                a.data.delivery.list = n;
                n.length<=0 && (a.data.delivery.empty = !0);
                a.setData({
                    delivery: a.data.delivery
                });
            }
        });
    },
    onPullDownRefresh: function() {
    },
    onReachBottom: function(e) {
    },
    onJsEvent: function(e) {
        t.util.jsEvent(e);
    }
});