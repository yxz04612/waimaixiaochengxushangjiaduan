var t = getApp();

Page({
    data: {
        notice: {
            page: 1,
            psize: 10,
            loaded: !1,
            empty: !1,
            data: []
        }
    },
    onLoad: function(t) {
        this.onPullDownRefresh();
    },
    onPullDownRefresh: function() {
        var t = this;
        t.setData({
            notice: {
                page: 1,
                psize: 10,
                loaded: !1,
                empty: !1,
                data: []
            }
        }), t.onReachBottom(), wx.stopPullDownRefresh();
    },
    onReachBottom: function(e) {
        var a = this;
        if (a.data.notice.loaded) return !1;
        t.util.request({
            url: "manage/delivery/delivery/user_list",
            data: {
                page: a.data.notice.page,
                psize: a.data.notice.psize
            },
            success: function(e) {
                var n = e.data.message;
                if (n.errno) return t.util.toast(n.message), !1;
                n = n.message;
                var o = a.data.notice.data.concat(n.user);
                a.data.notice.data = o, a.data.notice.page++, o.length || (a.data.notice.empty = !0),
                n.user.length < a.data.notice.psize && (a.data.notice.loaded = !0), a.setData({
                    notice: a.data.notice
                });
            }
        });
    },
    onJsEvent: function(e) {
        t.util.jsEvent(e);
    },
    switchChange:function(e){
        var index = e.currentTarget.dataset.index;
        var user_id = this.data.notice.data[index]['id'];
        var status = e.detail.value==true?1:0;
        var _this = this;
        t.util.request({
            url: "manage/delivery/delivery/user_status",
            // method: "POST",
            data:{status:status,id:user_id},
            success: function(e) {
                var n = e.data.message;
                wx.showToast({
                    title: n.message,
                    icon: 'success',
                    duration: 2000
                });
                _this.data.notice.data[index]['is_errander'] = status;
                _this.setData({
                    notice:_this.data.notice
                })
            }
        });
    },
    deleteDelivery:function(e){
        var index = e.currentTarget.dataset.index;
        var user_id = this.data.notice.data[index]['id'];
        var _this = this;
        wx.showModal({
            title: '提示',
            content: '确定要移除该配送员吗？',
            success: function (sm) {
                if (sm.confirm) {
                    t.util.request({
                        url: "manage/delivery/delivery/user_close",
                        // method: "POST",
                        data:{id:user_id},
                        success: function(e) {
                            var n = e.data.message;
                            wx.showToast({
                                title: n.message,
                                icon: 'success',
                                duration: 2000
                            });
                            delete _this.data.notice.data[index];
                            _this.setData({
                                notice:_this.data.notice
                            })
                        }
                    });
                } else if (sm.cancel) {
                    console.log('用户点击取消')
                }
            }
        });
    },
});