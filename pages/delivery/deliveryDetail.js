var t = getApp();

Page({
    data: {
        id:'',
        index: 0,
        time: '12:01',
        fee_type_item: [
            {name: 'distance', value: '里程计价'},
            {name: 'fee', value: '固定金额'},
        ],
        route_mode_item: [
            {name: 'riding', value: '按骑行路径'},
            {name: 'driving', value: '按驾车路径'},
            // {name: 'walking', value: '按步行路径'},
            // {name: 'line', value: '按直线路径'},
        ],
        page: {
            "type": "scene",
            "scene": "delivery",
            "title": "",
            "name": "",
            "thumb": "images\/21\/2019\/08\/k01860t620wyv1yuHqU1Jwyw11HWu1.jpg",
            "desc": "",
            "keyword": "",
            "background": "#F3F3F3",
            "diygotop": "0",
            "navigationbackground": "#000000",
            "navigationtextcolor": "#ffffff",
            "start_hour": "00:00",
            "end_hour": "23:59"
        },
        fees: {
            fee_day_limit: '2',
            fee_type: 'distance',
            fee_data: {
                fee: '5',
                distance: {
                    fee_is_int: '0',
                    route_mode: 'riding',
                    data: {
                        M0123456789100: {
                            start_hour: '00:00',
                            end_hour: '23:59',
                            start_km: '3',
                            start_fee: '8',
                            pre_km: '1',
                            pre_km_fee: '2',
                            over_km: '6',
                            over_pre_km: '1',
                            over_pre_km_fee: '3'
                        }
                    }
                },
                section: {
                    route_mode: 'riding',
                    data: {
                        M01234567891000: {
                            start_hour: '00:00',
                            end_hour: '23:59',
                            rules: {
                                data: {
                                    R0123456789100:{
                                        start_km: '0',
                                        end_km: '5',
                                        fee: '8'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            weight_status : 0,
            weight_data: {
                basic: 5,
                data: {
                    W0123456789100:{
                        over_kgs: '2',
                        pre_kg_fees: '3'
                    }
                }
            },
            extra_fee_time_status: 0,
            extra_fee_time_data: {
                data: {
                    E0123456789100:{
                        start_hour: '00:00',
                        end_hour: '01:00',
                        fee: '2'
                    }
                }
            },
            extra_fee: {
                C0123456789100:{
                    title: '',
                    min: 1,
                    max: 2,
                    status: 0,
                    data: {
                        C0123456789100: {
                            fee_name: '附加费1',
                            fee: 5
                        },
                        C0123456789101: {
                            fee_name: '附加费2',
                            fee: 2
                        }
                    }
                }
            }
        }
    },
    onLoad: function(option) {
        var a = this;
        a.setData({
            id: option.id,
        });
        t.util.request({
            url: "manage/delivery/delivery/detail",
            data:{id:option.id},
            success: function(e) {
                var n = e.data.message;
                if (n.errno) return t.util.toast(n.message), !1;
                n = n.message.detail;
                a.setData({
                    page: n.page,
                    fees: n.fees,
                });
            }
        });
    },
    bindTimeChange: function(e) {
        if(e.currentTarget.dataset.type == 'start'){
            this.data.page.start_hour = e.detail.value;
        }else{
            this.data.page.end_hour = e.detail.value;
        }
        this.setData({
            page: this.data.page
        })
    },
    bindDataTimeChange: function(e) {
        switch (e.currentTarget.dataset.class) {
            case "distance_data":
                var index = e.currentTarget.dataset.index;
                if(e.currentTarget.dataset.type == 'start'){
                    this.data.fees.fee_data.distance.data[index]['start_hour'] = e.detail.value;
                }else{
                    this.data.fees.fee_data.distance.data[index]['end_hour'] = e.detail.value;
                }
                break;
            case "extra_fee_time_data":
                var index = e.currentTarget.dataset.index;
                if(e.currentTarget.dataset.type == 'start'){
                    this.data.fees.extra_fee_time_data.data[index]['start_hour'] = e.detail.value;
                }else{
                    this.data.fees.extra_fee_time_data.data[index]['end_hour'] = e.detail.value;
                }
                break;

        }
        this.setData({
            fees: this.data.fees
        })
    },
    updateParams:function(e){
        var index = e.currentTarget.dataset.index;
        switch (e.currentTarget.dataset.class) {
            case "distance_data":
                this.data.fees.fee_data.distance.data[index][e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "weight_basic":
                this.data.fees.weight_data.basic = e.detail.value;
                break;
            case "weight_data":
                this.data.fees.weight_data.data[index][e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "extra_fee_time_data":
                this.data.fees.extra_fee_time_data.data[index][e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "extra_fee_data":
                this.data.fees.extra_fee[index][e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "extra_fee_data_data":
                this.data.fees.extra_fee.C0123456789100.data[index][e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "distance_data_local":
                this.data.fees[e.currentTarget.dataset.key] = e.detail.value;
                break;
            case "distance_data_fee":
                this.data.fees.fee_data[e.currentTarget.dataset.key] = e.detail.value;
                break;
        }
        this.setData({
            fees: this.data.fees
        })
    },
    radioExtraFeeChange: function(e) {
        this.data.fees.extra_fee_time_status = e.detail.value;
        this.setData({
            fees: this.data.fees
        })
    },
    radioFeeWeightChange: function(e) {
        this.data.fees.weight_status = e.detail.value;
        this.setData({
            fees: this.data.fees
        })
    },
    radioFeeTypeChange: function(e) {
        this.data.fees.fee_type = e.detail.value;
        this.setData({
            fees: this.data.fees
        })
    },
    radioExtraFeeTimeChange: function(e) {
      this.data.fees.extra_fee.C0123456789100.status = e.detail.value;
      this.setData({
        fees: this.data.fees
      })
    },
    radioRouteModeChange: function(e) {
        this.data.fees.fee_data.distance.route_mode = e.detail.value;
        this.setData({
            fees: this.data.fees
        })
    },
    addContent: function(e) {
        var type = e.currentTarget.dataset.type;
        switch (type) {
            case "distance_data":
                if(Object.keys(this.data.fees.fee_data.distance.data).length>=5){
                    wx.showToast({
                        title: '最多添加5个时间段',
                        icon: 'none',
                        duration: 2000
                    });
                    return ;
                }
                this.data.fees.fee_data.distance.data[this.getId('M', 0)] = {
                    start_hour: '00:00',
                    end_hour: '23:59',
                    start_km: '3',
                    start_fee: '8',
                    pre_km: '1',
                    pre_km_fee: '2',
                    over_km: '6',
                    over_pre_km: '1',
                    over_pre_km_fee: '3'
                };
                break;
            case "weight_data":
                var clone_weight_data = this.data.fees.weight_data.data;
                if(Object.keys(this.data.fees.weight_data.data).length>=3){
                    wx.showToast({
                        title: '最多添加3个重量计费',
                        icon: 'none',
                        duration: 2000
                    });
                    return ;
                }
                var end_data_over_kgs = clone_weight_data[Object.keys(clone_weight_data).pop()]['over_kgs'];
                this.data.fees.weight_data.data[this.getId('W', 0)] = {
                    over_kgs: parseInt(end_data_over_kgs)+1,
                    pre_kg_fees: 3
                };
                break;
            case "extra_fee_time_data":
                if(Object.keys(this.data.fees.extra_fee_time_data.data).length>=5){
                    wx.showToast({
                        title: '最多添加5个时段附加',
                        icon: 'none',
                        duration: 2000
                    });
                    return ;
                }
                this.data.fees.extra_fee_time_data.data[this.getId('E', 0)] = {
                    start_hour: '21:00',
                    end_hour: '23:00',
                    fee: '2'
                };
                break;
            case "extra_fee_data_data":
                if(Object.keys(this.data.fees.extra_fee.C0123456789100.data).length>=5){
                    wx.showToast({
                        title: '最多添加5个附加费',
                        icon: 'none',
                        duration: 2000
                    });
                    return ;
                }
                this.data.fees.extra_fee.C0123456789100.data[this.getId('C', 0)] = {
                    fee_name: '',
                    fee: 5
                };
                break;
        }

        this.setData({
            fees: this.data.fees
        })
    },
    deleteDistanceData:function(e){
        if(Object.keys(this.data.fees.fee_data.distance.data).length==1){
            wx.showToast({
                title: '至少保留1个时间段',
                icon: 'none',
                duration: 2000
            });
            return ;
        }
        var index = e.currentTarget.dataset.index;
        delete this.data.fees.fee_data.distance.data[index];
        this.setData({
            fees: this.data.fees
        })
    },
    deleteWeightData:function(e){
        if(Object.keys(this.data.fees.weight_data.data).length==1){
            wx.showToast({
                title: '至少保留1个重量计费',
                icon: 'none',
                duration: 2000
            });
            return ;
        }
        var index = e.currentTarget.dataset.index;
        delete this.data.fees.weight_data.data[index];
        this.setData({
            fees: this.data.fees
        })
    },
    deleteExtraFeeTimeData:function(e){
        if(Object.keys(this.data.fees.extra_fee_time_data.data).length==1){
            wx.showToast({
                title: '至少保留1个时段附加',
                icon: 'none',
                duration: 2000
            });
            return ;
        }
        var index = e.currentTarget.dataset.index;
        delete this.data.fees.extra_fee_time_data.data[index];
        this.setData({
            fees: this.data.fees
        })
    },
    deleteExtraFeeDataData:function(e){
        if(Object.keys(this.data.fees.extra_fee.C0123456789100.data).length==1){
            wx.showToast({
                title: '至少保留1个附加费',
                icon: 'none',
                duration: 2000
            });
            return ;
        }
        var index = e.currentTarget.dataset.index;
        delete this.data.fees.extra_fee.C0123456789100.data[index];
        this.setData({
            fees: this.data.fees
        })
    },
    postData:function(){
        var _this = this;
        void t.util.request({
            url: "manage/delivery/delivery/post",
            // method: "POST",
            data:{page:JSON.stringify(_this.data.page),fees:JSON.stringify(_this.data.fees),id:_this.data.id},
            success: function(e) {
                var n = e.data.message;
                wx.showToast({
                    title: n.message,
                    icon: 'success',
                    duration: 2000
                });
                wx.navigateBack();
            }
        });
    },
    getId: function(S, N) {
        var date = +new Date();
        var id = S + (date + N);
        return id;
    }
});