import * as echarts from '../../ec-canvas/echarts'; //引入echarts.js
var dataList = [];
var k = 0;
var Chart = null;
var t = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        ec: {
            lazyLoad: true // 延迟加载
        },
        total_fee:0
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    onShow: function () {
        Chart = null;
        this.echartsComponnet = this.selectComponent('#mychart');
        this.getData('week'); //获取数据
    },
    getData: function (type) {
        var _this = this;
        var days = 7;
        switch (type){
            case 'week':
                days = 7;
                break;
            case 'month':
                days = 30;
                break;
            case 'quarter':
                days = -10;
                break;
        }
        /**
         * 此处的操作：
         * 获取数据json
         */
        // if(k % 2){
        //     dataList = [1, 2, 3, 4, 5, 6];
        // }else{
        //     dataList = [7, 6, 9, 2, 5, 6];
        // }
        // k++;

        //如果是第一次绘制
        // if (!Chart){
        //     this.init_echarts(); //初始化图表
        // }else{
        //     this.setOption(Chart); //更新数据
        // }
        t.util.request({
            url: "manage/order/statistics/qs",
            data: {
                days: days
            },
            success: function(e) {
                var data = e.data.message.message;
                dataList = data.list;
                _this.setData({
                    total_fee: data.total_fee
                });
                if (!Chart){
                    _this.init_echarts(); //初始化图表
                }else{
                    _this.setOption(Chart); //更新数据
                }
            }
        });
        // wx.request({
        //   url: url, //仅为示例，并非真实的接口地址
        //   data: data,
        //   method: 'POST',
        //   header: { 'content-type': 'application/x-www-form-urlencoded' },
        //   success: (res) => {
        //     dataList = res.data;
        //       if (!Chart){
        //           this.init_echarts(); //初始化图表
        //       }else{
        //           this.setOption(Chart); //更新数据
        //       }
        //   }
        // });
    },
    //初始化图表
    init_echarts: function () {
        this.echartsComponnet.init((canvas, width, height) => {
            // 初始化图表
            Chart = echarts.init(canvas, null, {
                width: width,
                height: height
            });
            // Chart.setOption(this.getOption());
            this.setOption(Chart);
            // 注意这里一定要返回 chart 实例，否则会影响事件处理等
            return Chart;
        });
    },
    setOption: function (Chart) {
        Chart.clear();  // 清除
        Chart.setOption(this.getOption());  //获取新数据
    },
    getOption: function () {
        // 指定图表的配置项和数据
        var option = {
            backgroundColor: "#ffffff",
            // color: ["#37A2DA", "#32C5E9", "#67E0E3", "#91F2DE", "#FFDB5C", "#FF9F7F"],
            series: [{
                label: {
                    normal: {
                        fontSize: 14
                    }
                },
                type: 'pie',
                center: ['50%', '50%'],
                radius: [0, '60%'],
                data: dataList,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 2, 2, 0.3)'
                    }
                }
            }]
        };
        return option;
    },
    init:function (e) {
        this.getData(e.currentTarget.dataset.type);
    }
});
