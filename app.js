App({
    onLaunch: function() {
        console.log("=================onLaunch==================");
    },
    onShow: function() {
        console.log("=================onShow==================");
    },
    onHide: function() {},
    util: require("static/js/utils/util.js"),
    WxParse: require("./library/wxParse/wxParse.js"),
    ext: {
        siteInfo: {
            uniacid: "2",
            acid: "2",
          siteroot: "https://run.limetime.cn/app/wxapp.php",
          sitebase: "https://run.limetime.cn/app",
            // siteroot: "http://www.dbg.com/app/wxapp.php",
            // sitebase: "http://www.dbg.com/app",
            module: "we7_wmall"
        }
    },
    navigator: {
        list: [ {
            link: "pages/order/index",
            icon: "icon-order"
        }, {
            link: "pages/order/tangshi/index",
            icon: "icon-order"
        }, {
            link: "pages/shop/home",
            icon: "icon-mine"
        }, {
            link: "pages/shop/setting",
            icon: "icon-mine"
        } ],
        position: {
            bottom: "80px"
        }
    }
});